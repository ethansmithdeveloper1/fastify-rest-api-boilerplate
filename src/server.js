// Setup Fastify Framework
const fastify = require('fastify')({
    logger: true,
    // https: true
});

// //Register TLS
// fastify.register(require('fastify-tls-keygen'), {
//     key: './tls/key.pem',
//     cert: './tls/certificate.pem'
// })

// Import Swagger Options
const swagger = require('./config/swagger');
// Register Swagger
fastify.register(require('fastify-swagger'), swagger.options);

// Setup Database Connection
const database = require('./database.js');

const port = 3000;


//Setup Routes
const routes = require('./routes/modelRoutes.js')
routes.forEach((route, index) => {
    fastify.route(route)
})

// Run the server!
const start = async () => {
    try {
        await fastify.listen(port);
        fastify.swagger();
        fastify.log.info(`server listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    };
};
start();