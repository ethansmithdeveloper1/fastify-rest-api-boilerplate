// Import Mongoose
const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
  name: String,
});

module.exports = mongoose.model('Model', modelSchema);