// Import Controllers
const modelController = require('../controllers/modelController');

const routes = [
  {
    method: 'GET',
    url: '/api/models',
    handler: modelController.getModels
  },
  {
    method: 'GET',
    url: '/api/models/:id',
    handler: modelController.getSingleModel
  },
  {
    method: 'POST',
    url: '/api/models',
    handler: modelController.addModel,
  },
  {
    method: 'PUT',
    url: '/api/models/:id',
    handler: modelController.updateModel
  },
  {
    method: 'DELETE',
    url: '/api/models/:id',
    handler: modelController.deleteModel
  }
];

module.exports = routes;